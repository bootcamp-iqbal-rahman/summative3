package id.co.nexsoft.summative3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import id.co.nexsoft.summative3.model.Lesson;
import jakarta.transaction.Transactional;

public interface LessonRepository extends JpaRepository<Lesson, Integer> {
    @Query("SELECT l FROM Lesson l")
    List<Lesson> getAllData();

    @Query("SELECT l FROM Lesson l WHERE l.id = :id")
    Lesson getDataById(int id);

    @Modifying
    @Transactional
    @Query("DELETE FROM Lesson WHERE id = :id")
    void deleteDataById(int id);
}