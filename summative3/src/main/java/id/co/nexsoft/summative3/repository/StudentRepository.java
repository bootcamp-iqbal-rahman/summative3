package id.co.nexsoft.summative3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import id.co.nexsoft.summative3.model.Student;
import jakarta.transaction.Transactional;

public interface StudentRepository extends JpaRepository<Student, Integer> {
    @Query("SELECT s FROM Student s")
    List<Student> getAllData();

    @Query("SELECT s FROM Student s WHERE s.id = :id")
    Student getDataById(int id);

    @Modifying
    @Transactional
    @Query("DELETE FROM Student WHERE id = :id")
    void deleteDataById(int id);
}