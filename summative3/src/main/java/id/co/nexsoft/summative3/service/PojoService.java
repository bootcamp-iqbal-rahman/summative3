package id.co.nexsoft.summative3.service;

public interface PojoService<T> {
    T addData(T data);
    T putData(T data, int id);
}
