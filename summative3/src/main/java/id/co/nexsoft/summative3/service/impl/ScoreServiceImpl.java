package id.co.nexsoft.summative3.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.summative3.model.Score;
import id.co.nexsoft.summative3.repository.ScoreRepository;
import id.co.nexsoft.summative3.service.DefaultService;
import id.co.nexsoft.summative3.service.DynamicService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class ScoreServiceImpl implements DefaultService<Score>, DynamicService {
    @Autowired
    private ScoreRepository repository;

    @Autowired
    private EntityManager entityManager;
    
    @Override
    public List<Score> getAllData() {
        return repository.getAllData();
    }

    @Override
    public Score getDataById(int id) {
        return repository.getDataById(id);
    }

    @Override
    public void deleteDataById(int id) {
        repository.deleteDataById(id);
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, int id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE score SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    public void addData(Map<String, Object> data) {
        repository.addData(data);
    }

    @Override
    public void putData(Map<String, Object> data, int id) {
        repository.updateData(data, id);
    }
}