package id.co.nexsoft.summative3.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.summative3.model.Student;
import id.co.nexsoft.summative3.service.DefaultService;
import id.co.nexsoft.summative3.service.PojoService;
import id.co.nexsoft.summative3.util.StatusCode;

@RestController
@RequestMapping("/api/student")
public class StudentController extends StatusCode {
    @Autowired
    private DefaultService<Student> defaultServices;

    @Autowired
    private PojoService<Student> pojoService;

    @GetMapping
    public ResponseEntity<?> getAllData() {
        List<Student> student = defaultServices.getAllData();
        return new ResponseEntity<>(student, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getDataById(@PathVariable int id) {
        Student student = defaultServices.getDataById(id);
        if (student == null) {
            return new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND);
        } 
        return new ResponseEntity<>(student, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> addData(@RequestBody Student data) {
        pojoService.addData(data);
        return new ResponseEntity<>(get201(), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putData(@PathVariable int id, @RequestBody Student data) {
        pojoService.putData(data, id);
        return new ResponseEntity<>(get201(), HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public void patchData(@PathVariable int id, @RequestBody Map<String, Object> data) {
        defaultServices.patchData(data, id);
    }

    @DeleteMapping("/{id}")
    public void deleteDataById(@PathVariable int id) {
        defaultServices.deleteDataById(id);
    }
}