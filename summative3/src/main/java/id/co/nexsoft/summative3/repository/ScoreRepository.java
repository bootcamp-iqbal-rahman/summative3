package id.co.nexsoft.summative3.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.summative3.model.Score;
import jakarta.transaction.Transactional;

public interface ScoreRepository extends JpaRepository<Score, Integer> {
    @Query("SELECT s FROM Score s")
    List<Score> getAllData();

    @Query("SELECT s FROM Score s WHERE s.id = :id")
    Score getDataById(int id);

    @Modifying
    @Transactional
    @Query("DELETE FROM Score WHERE id = :id")
    void deleteDataById(int id);

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO score (student_id, lesson_id, score) VALUES (:#{#data['student_id']}, :#{#data['lesson_id']}, :#{#data['score']})", nativeQuery = true)
    void addData(@Param("data") Map<String, Object> data);

    @Transactional
    @Modifying
    @Query(value = "UPDATE score SET student_id = :#{#data['student_id']}, lesson_id = :#{#data['lesson_id']}, score = :#{#data['score']} WHERE id = :id", nativeQuery = true)
    void updateData(@Param("data") Map<String, Object> data, @Param("id") int id);
}