package id.co.nexsoft.summative3.service;

import java.util.Map;

public interface DynamicService {
    void addData(Map<String, Object> data);
    void putData(Map<String, Object> data, int id);
}
