package id.co.nexsoft.summative3.util;

import java.util.HashMap;
import java.util.Map;

public class StatusCode {
    static Map<String, Object> STATUS_CODE = new HashMap<>();
    static String[] KEY = {
        "response", "message"
    };
    static int[] CODE = {
        404, 400, 201, 200, 422
    };
    static String[] MESSAGE = {
        "Data not found", "Bad request", "Data created successfully",
        "Success", "Unprocessable Entity"
    };

    public StatusCode() {}

    public static Map<String, Object> get404() {
        return setStatus(0);
    }

    public static Map<String, Object> get201() {
        return setStatus(2);
    }

    public static Map<String, Object> setStatus(int id) {
        STATUS_CODE.put(KEY[0], CODE[id]);
        STATUS_CODE.put(KEY[1], MESSAGE[id]);
        return STATUS_CODE;
    }
}
