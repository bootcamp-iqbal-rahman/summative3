package id.co.nexsoft.summative3.service;

import java.util.List;
import java.util.Map;

public interface DefaultService<T> {
    List<T> getAllData();
    T getDataById(int id);
    void deleteDataById(int id);
    void patchData(Map<String, Object> data, int id);
}
