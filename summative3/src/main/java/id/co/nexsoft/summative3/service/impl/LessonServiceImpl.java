package id.co.nexsoft.summative3.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

import id.co.nexsoft.summative3.model.Lesson;
import id.co.nexsoft.summative3.repository.LessonRepository;
import id.co.nexsoft.summative3.service.DefaultService;
import id.co.nexsoft.summative3.service.PojoService;

@Service
public class LessonServiceImpl implements DefaultService<Lesson>, PojoService<Lesson> {
    @Autowired
    private LessonRepository repository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Lesson> getAllData() {
        return repository.getAllData();
    }

    @Override
    public Lesson getDataById(int id) {
        return repository.getDataById(id);
    }

    @Override
    public void deleteDataById(int id) {
        repository.deleteDataById(id);
    }

    @Override
    public Lesson addData(Lesson data) {
        return repository.save(data);
    }

    @Override
    public Lesson putData(Lesson data, int id) {
        data.setId(id);
        return repository.save(data);
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, int id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE lesson SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }
}