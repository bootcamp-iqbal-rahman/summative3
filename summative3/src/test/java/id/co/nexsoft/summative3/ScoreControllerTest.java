package id.co.nexsoft.summative3;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import id.co.nexsoft.summative3.model.Lesson;
import id.co.nexsoft.summative3.model.Score;
import id.co.nexsoft.summative3.model.Student;

@SpringBootTest
@AutoConfigureMockMvc
class ScoreControllerTest {

	@Autowired
	private MockMvc mockMvc;

    @Test
    void getNullDataById() throws Exception {
        this.mockMvc.perform(get("/api/score/1000"))
            .andDo(print())
            .andExpect(status().isNotFound());
    }

    @Test
    void addData() throws Exception {
        Map<String, Object> data = new HashMap<>();
        data.put("student_id", 3);
        data.put("lesson_id", 4);
        data.put("score", 100.0);

        mockMvc.perform(post("/api/score")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(data))
            ).andExpect(status().isCreated());
    }

	@Test
	void getAllData() throws Exception {
		this.mockMvc.perform(get("/api/score"))
			.andDo(print())
			.andExpect(status().isOk());
	}

    @Test
    void getDataById() throws Exception {
        this.mockMvc.perform(get("/api/score/3"))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void putData() throws Exception {
        Map<String, Object> data = new HashMap<>();
        data.put("student_id", 1);
        data.put("lesson_id", 1);
        data.put("score", 100.0);

        mockMvc.perform(put("/api/score/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(data))
            ).andExpect(status().isCreated());
    }

    @Test
    void patchData() throws Exception {
        Map<String, Object> data = new HashMap<>();
        data.put("score", 89.0);

        mockMvc.perform(patch("/api/score/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(data))
            ).andExpect(status().isOk());
    }

    @Test
    void deleteData() throws Exception {
        mockMvc.perform(delete("/api/score/{id}", 2)
                ).andExpect(status().isOk());
    }

    private String asJsonString(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper.writeValueAsString(object);
    }

    @Test
    void addPojoData() {
        Student student = new Student(
            "Bambang", "Sugeni", LocalDate.now(), "Male"
        );
        Lesson lesson = new Lesson(
            "Bio", 4
        );
        Score score = new Score(student, lesson, 100.0);
        score.setId(100);
        score.setStudent(student);
        score.setLesson(lesson);
        score.setScore(100.0);
    }
}