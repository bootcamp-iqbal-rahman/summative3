package id.co.nexsoft.summative3;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import id.co.nexsoft.summative3.model.Lesson;

@SpringBootTest
@AutoConfigureMockMvc
class LessonControllerTest {

	@Autowired
	private MockMvc mockMvc;

    @Test
    void getNullAllData() throws Exception {
        this.mockMvc.perform(get("/api/lesson"))
			.andDo(print())
			.andExpect(status().isOk());
    }

    @Test
    void getNullDataById() throws Exception {
        this.mockMvc.perform(get("/api/lesson/1000"))
            .andDo(print())
            .andExpect(status().isNotFound());
    }

    @Test
    void addData() throws Exception {
        Lesson data = new Lesson(
            "Mathematics", 4
        );

        mockMvc.perform(post("/api/lesson")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(data))
            ).andExpect(status().isCreated());
    }

    @Test
    void putData() throws Exception {
        Lesson data = new Lesson(
            "Mathematics", 5
        );

        mockMvc.perform(put("/api/lesson/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(data))
            ).andExpect(status().isCreated());
    }

    @Test
    void patchData() throws Exception {
        Map<String, Object> patchData = new HashMap<>();
        patchData.put("name", "Mathematics");
        patchData.put("level", 5);

        mockMvc.perform(patch("/api/lesson/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(patchData))
            ).andExpect(status().isOk());
    }

    @Test
	void getAllData() throws Exception {
		this.mockMvc.perform(get("/api/lesson"))
			.andDo(print())
			.andExpect(status().isOk());
	}

    @Test
    void getDataById() throws Exception {
        this.mockMvc.perform(get("/api/lesson/1"))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void deleteData() throws Exception {
        mockMvc.perform(delete("/api/lesson/{id}", 5)
                ).andExpect(status().isOk());
    }

    private String asJsonString(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper.writeValueAsString(object);
    }
}