package id.co.nexsoft.summative3;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import id.co.nexsoft.summative3.model.Student;

@SpringBootTest
@AutoConfigureMockMvc
class StudentControllerTest {

	@Autowired
	private MockMvc mockMvc;

    @Test
    void getNullAllData() throws Exception {
        this.mockMvc.perform(get("/api/student"))
			.andDo(print())
			.andExpect(status().isOk());
    }

    @Test
    void getNullDataById() throws Exception {
        this.mockMvc.perform(get("/api/student/1000"))
            .andDo(print())
            .andExpect(status().isNotFound());
    }

    @Test
    void putNullData() throws Exception {
        Student student = new Student(
            "M. Bambang", "Sugeni", LocalDate.now(), "Male"
        );

        mockMvc.perform(put("/api/student/{id}", 130)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(student))
            ).andExpect(status().isCreated());
    }

    @Test
    void addData() throws Exception {
        Student student = new Student(
            "bambang", "Sugeni", LocalDate.now(), "Male"
        );

        mockMvc.perform(post("/api/student")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(student))
            ).andExpect(status().isCreated());
    }

    @Test
    void putData() throws Exception {
        Student student = new Student(
            "M. Bambang", "Sugeni", LocalDate.now(), "Male"
        );

        mockMvc.perform(put("/api/student/{id}", 13)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(student))
            ).andExpect(status().isCreated());
    }

    @Test
    void patchData() throws Exception {
        Map<String, Object> patchData = new HashMap<>();
        patchData.put("name", "Bambang");
        patchData.put("surname", "Syaironi");

        mockMvc.perform(patch("/api/student/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(patchData))
            ).andExpect(status().isOk());
    }
    
	@Test
	void getAllData() throws Exception {
		this.mockMvc.perform(get("/api/student"))
			.andDo(print())
			.andExpect(status().isOk());
	}

    @Test
    void getDataById() throws Exception {
        this.mockMvc.perform(get("/api/student/2"))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void deleteData() throws Exception {
        mockMvc.perform(delete("/api/student/{id}", 4)
                ).andExpect(status().isOk());
    }

    private String asJsonString(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper.writeValueAsString(object);
    }
}