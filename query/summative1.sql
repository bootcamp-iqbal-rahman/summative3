CREATE DATABASE summativedb;

CREATE TABLE student (
  id int NOT NULL AUTO_INCREMENT,
  name varchar(50),
  surname varchar(50),
  birthdate date,
  gender varchar(10),
  CONSTRAINT pk_student PRIMARY KEY (id)
);

CREATE TABLE lesson (
  id int NOT NULL AUTO_INCREMENT,
  name varchar(100),
  level int,
  CONSTRAINT pk_lesson PRIMARY KEY (id)
);

CREATE TABLE score (
  id int NOT NULL AUTO_INCREMENT,
  student_id int,
  lesson_id int,
  score double,
  CONSTRAINT pk_score PRIMARY KEY (id),
  CONSTRAINT fk_student_id FOREIGN KEY (student_id) REFERENCES student(id),
  CONSTRAINT fk_lesson_id FOREIGN KEY (lesson_id) REFERENCES lesson(id)
);

INSERT INTO student
	(name, surname, birthdate, gender)
VALUES
	('Iqbal', 'Rahman', '2003-01-07', 'Male'),
    ('Alya', 'Nur', '1995-05-15', 'Female'),
    ('Rizki', 'Pratama', '1988-09-22', 'Male'),
    ('Dewi', 'Sari', '2000-12-10', 'Female'),
    ('Aditya', 'Wijaya', '1992-03-18', 'Male'),
    ('Siti', 'Rahayu', '1985-07-02', 'Female'),
    ('Hadi', 'Santoso', '1998-11-25', 'Male'),
    ('Lina', 'Fitriani', '1983-04-30', 'Female'),
    ('Fauzi', 'Akbar', '1990-08-12', 'Male'),
    ('Rina', 'Wulandari', '2005-02-14', 'Female'),
    ('Budi', 'Saputra', '1997-06-08', 'Male')
;
    
INSERT INTO lesson
	(name, level)
VALUES
	('Mathematics', 1),
    ('Science', 1),
    ('English', 1),
    ('Biologi', 1),
    ('Physics', 1),
	('Mathematics', 2),
    ('Science', 2),
    ('English', 2),
    ('Biologi', 2),
    ('Physics', 2),
	('Mathematics', 3),
    ('Science', 3),
    ('English', 3),
    ('Biologi', 3),
    ('Physics', 3)
 ;
 
 INSERT into score 
 	(student_id, lesson_id, score)
 VALUES
 	(1, 1, 92.0),
    (1, 2, 90.0),
    (1, 3, 89.0),
    (1, 4, 80.0),
    (1, 5, 96.0),
    (2, 1, 88.0),
    (2, 2, 91.0),
    (2, 3, 85.0),
    (2, 4, 78.0),
    (2, 5, 94.0),
    (3, 1, 90.5),
    (3, 2, 89.0),
    (3, 3, 92.0),
    (3, 4, 85.5),
    (3, 5, 88.0),
    (4, 1, 85.0),
    (4, 2, 88.5),
    (4, 3, 91.0),
    (4, 4, 82.5),
    (4, 5, 87.0),
    (5, 1, 89.5),
    (5, 2, 90.0),
    (5, 3, 88.0),
    (5, 4, 87.5),
    (5, 5, 93.0),
    (6, 1, 91.0),
    (6, 2, 87.0),
    (6, 3, 89.5),
    (6, 4, 80.0),
    (6, 5, 92.0),
    (7, 1, 88.5),
    (7, 2, 92.0),
    (7, 3, 90.0),
    (7, 4, 86.5),
    (7, 5, 87.0),
    (8, 1, 89.0),
    (8, 2, 91.5),
    (8, 3, 88.0),
    (8, 4, 85.5),
    (8, 5, 90.0),
    (9, 1, 87.5),
    (9, 2, 89.0),
    (9, 3, 91.0),
    (9, 4, 84.5),
    (9, 5, 88.0),
    (10, 1, 90.0),
    (10, 2, 88.5),
    (10, 3, 87.0),
    (10, 4, 86.5),
    (10, 5, 91.0),
    (11, 1, 88.0),
    (11, 2, 90.5),
    (11, 3, 89.0),
    (11, 4, 87.5),
    (11, 5, 92.0)
;