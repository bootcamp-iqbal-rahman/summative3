SELECT
	ST.name AS 'First Name',
    ST.surname AS 'Last Name',
    L.name AS 'Lesson Name',
    L.level AS 'level',
    S.score AS 'Score'
FROM
	score S
LEFT JOIN
	student ST
ON
	S.student_id = ST.id
LEFT JOIN
	lesson L
ON
	S.lesson_id = L.id
;